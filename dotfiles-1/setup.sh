#!/bin/bash
dotfilesdir=$( dirname "$( readlink -f $0 )" )
echo "dotfiles at $dotfilesdir"

if [ ! -d $HOME/.vim/bundle/Vundle.vim ]; then
    git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
fi

if [ ! -e $HOME/.vimrc ]; then
    ln -s $dotfilesdir/vimrc $HOME/.vimrc
fi

