inp = open("oracle_columns_types.txt", "r")
out = open("view.txt", "w")

# Types definition
STRING = "string"
DATE = "time.Time"
CHAR = "byte"
NUMBER = "int"

def to_camel_case(snake_str):
    components = snake_str.split('_')
    return components[0] + ''.join(x.title() for x in components[1:])

out.write("package normalization\n\n")
out.write("// DMSView ...\n")
out.write("type DMSView struct {\n")
for line in inp.readlines():
    type_ = line.split('type: ')[1].replace('\n', '')
    attr = line.split(': ')[1].split(',')[0]

    if type_ == 'VARCHAR2':
        out.write("\t {} {}\n".format(to_camel_case(attr), STRING))
    elif type_ == 'DATE':
        out.write("\t {} {}\n".format(to_camel_case(attr), DATE))
    elif type_ == 'CHAR':
        out.write("\t {} {}\n".format(to_camel_case(attr), CHAR))
    elif type_ == 'NUMBER':
        out.write("\t {} {}\n".format(to_camel_case(attr), NUMBER))
    else:
        print("ERROR:", attr)
out.write("}\n")