import re

f = open("cols.txt", "r")
output = open("new_cols.txt", "w")

def camel(s):
    return s != s.lower() and s != s.upper() and "_" not in s

def convert(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).upper()

def capitalizeWords(s):
  return re.sub(r'\w+', lambda m:m.group(0).capitalize(), s)

for line in f.readlines():
  print(capitalizeWords(line))
    # if not camel(line):
    #     output.write(line.upper() + ", ")
    # else:
    #     output.write(convert(line) + ", ")

f.close()
output.close()
