form = """`form:"{}" json:"{}" yaml:"{}" xml:"{}"`"""

import re

f = open("cols.txt", "r")
output = open("new_cols.txt", "w")

def sliceindex(x):
    i = 0
    for c in x:
        if c.isalpha():
            i = i + 1
            return i
        i = i + 1

def upperfirst(x):
    i = sliceindex(x)
    return x[:i].upper() + x[i:]

for line in f.readlines():
  name = line.split(" ")[0].replace(" ", "")
  print(name)

  new_name = upperfirst(name)

  new_line = new_name + ": rv.{}".format(new_name) + ",\n"
  print(new_line)
  output.write(new_line)

f.close()
output.close()
