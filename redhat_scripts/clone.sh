repos_list="sma-activity-log sma-disambiguation sma-mockdata sma-setup sma-activity-logstash sma-dms-adapter sma-ocp-bootstrap sma-template-adapter sma-backend sma-file-importer sma-oracle-adapter sma-tests sma-base-images sma-fuse-subscriptions sma-pipelines sma-topic-detection sma-benchmark sma-geo-api sma-pms-adapter sma-twitter-adapter sma-bootstrap sma-graph-api sma-query-builder sma-web sma-datasources sma-graph-demo sma-resource-api sma-dimensions sma-int-test sma-sentiment-analysis"
for repo in $repos_list; do
  echo $repo
  git clone "http://10.100.183.100/sma/$repo.git"
done