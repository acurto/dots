PYTHON_EXTENSIONS=("cssho.vscode-svgviewer" \
  "dbaeumer.vscode-eslint")
JAVA_EXTENSIONS=("cssho.vscode-svgviewer" \
  "dbaeumer.vscode-eslint")
# HASKELL_EXTENSIONS=()
# GOLANG_EXTENSIONS=()
# C_EXTENSIONS=()
# FORTRAN_EXTENSIONS=()
# GIT_EXTENSIONS=()
# DOCKER_EXTENSIONS=()
# DBS_EXTENSIONS=()

EXTENSIONS=("${JAVA_EXTENSIONS[@]}"\
  "${PYTHON_EXTENSIONS[@]}"
  )

for EXTENSION in ${EXTENSIONS[@]}
  do
    echo "Installing extension: $EXTENSION"
    code --install-extension $EXTENSION
    echo "Done!" $"\n"
  done