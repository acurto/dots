#################### Exports ####################
# If you come from bash you might have to change your $PATH.
export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=/Users/agustincurto/.oh-my-zsh


export PATH="/usr/local/opt/ncurses/bin:$PATH"

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes

#################### OhMyZSH Configurations ####################
ZSH_THEME="robbyrussell"

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
  git
)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"


#################### Personal aliases ####################

# Developer aliases
alias dev="cd /Users/agustincurto/Documents/dev"
alias brewall="brew update && brew upgrade && brew cleanup && brew doctor"
alias jn="jupyter notebook"
alias zx81="ssh cp201806@zx81.famaf.unc.edu.ar"

# Degree aliases
alias degree="cd /Users/agustincurto/Documents/faculty/degree"
alias fisica="cd /Users/agustincurto/Documents/faculty/degree/fourth/2/fisica"
alias comp="cd /Users/agustincurto/Documents/faculty/degree/fifth/compiladores"
alias thesis="cd /Users/agustincurto/Documents/faculty/degree/thesis"

# Teaching aliases
alias so="cd /Users/agustincurto/Documents/faculty/teaching/so2018"
alias teach="cd /Users/agustincurto/Documents/faculty/teaching"

# Work aliases
alias work="cd /Users/agustincurto/Documents/work"
alias jmeter="java -jar ApacheJMeter.jar"
alias test="cd /Users/agustincurto/Documents/work/aCO_Web_QAAT/tests"
alias server="cd /Users/agustincurto/Documents/work/aCO_Server"
alias web="cd /Users/agustincurto/Documents/work/aCO_Web"

# Other aliases
alias rmpermissions1="sudo xattr -rc"
alias rmpermissions2="sudo chmod -R -N"



########## Clusters access aliases ##########
# Degree
alias zx81="ssh cp201806@zx81.famaf.unc.edu.ar"
alias mendieta="ssh acurto@mendieta.ccad.unc.edu.ar"
alias eulogia="ssh acurto@eulogia.ccad.unc.edu.ar"
alias mulatona="ssh acurto@mulatona.ccad.unc.edu.ar"

# Work
alias 59="ssh root@192.168.110.59"
alias 69="ssh root@192.168.110.69"
alias 79="ssh root@192.168.110.79"
alias 89="ssh root@192.168.110.89"
alias 99="ssh root@192.168.110.99"
